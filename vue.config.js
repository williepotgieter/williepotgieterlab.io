const routes = require('./src/routes');

module.exports = {
	css: {
		loaderOptions: {
			sass: {
				prependData: `
				@import "@/assets/scss/bulma.custom.scss";
				@import "@/assets/scss/custom.utils.scss";
				`
			}
		}
	},
	chainWebpack: config => {
		config.plugin('html').tap(args => {
			args[0].title = 'Willie Potgieter || Freelance full-stack web developer';
			return args;
		});
	},
	pluginOptions: {
		sitemap: {
			baseURL: 'https://williepotgieter.gitlab.io',
			routes
		}
	}
};

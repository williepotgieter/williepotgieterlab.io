module.exports = [
	{
		path: '/',
		name: 'Home',
		component: () => import('./views/Home.vue'),
		meta: {
			sitemap: {
				lastmod: 'April 9, 2020',
				priority: 0.9,
				changefreq: 'weekly',
			},
		},
	},
	{
		path: '/services',
		name: 'Services',
		component: () => import('./views/Services.vue'),
		meta: {
			sitemap: {
				lastmod: 'March 26, 2020',
				priority: 1.0,
				changefreq: 'weekly',
			},
		},
	},
	{
		path: '/contact',
		name: 'Contact',
		component: () => import('./views/Contact.vue'),
		meta: {
			sitemap: {
				lastmod: 'March 26, 2020',
				priority: 0.2,
				changefreq: 'weekly',
			},
		},
	},
];

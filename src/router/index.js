import Vue from 'vue';
import VueRouter from 'vue-router';
import routes from '../routes';

Vue.use(VueRouter);

// const routes = [
// 	{
// 		path: '/',
// 		name: 'Home',
// 		// route level code-splitting
// 		// this generates a separate chunk (about.[hash].js) for this route
// 		// which is lazy-loaded when the route is visited.
// 		component: () => import(/* webpackChunkName: "about" */ '../views/Home.vue')
// 	},
// 	{
// 		path: '/services',
// 		name: 'Services',
// 		component: () => import('../views/Services.vue')
// 	},
// 	// {
// 	// 	path: '/projects',
// 	// 	name: 'Projects',
// 	// 	component: () => import('../views/Projects.vue')
// 	// },
// 	{
// 		path: '/contact',
// 		name: 'Contact',
// 		component: () => import('../views/Contact.vue')
// 	}
// ];

const router = new VueRouter({
	mode: 'history',
	base: process.env.BASE_URL,
	routes
});

export default router;

import Vue from 'vue';
import App from './App.vue';
import router from './router';
import Buefy from 'buefy';
import VueMq from 'vue-mq';
import Vuelidate from 'vuelidate';
import VueAnalytics from 'vue-analytics';

Vue.use(VueAnalytics, {
	id: 'UA-143321782-2',
	router
});

// Font Awesome
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import {
	faCheck,
	faEnvelope,
	faCode,
	faCubes,
	faMicrochip,
	faExclamationCircle,
	faCheckCircle,
	faCogs,
	faSuitcase
} from '@fortawesome/free-solid-svg-icons';
import { faWindowRestore } from '@fortawesome/free-regular-svg-icons';
import {
	faLinkedinIn,
	faLinkedin,
	faTelegramPlane,
	faGitlab
} from '@fortawesome/free-brands-svg-icons';
library.add(
	faCheck,
	faCode,
	faLinkedinIn,
	faEnvelope,
	faTelegramPlane,
	faLinkedin,
	faWindowRestore,
	faCubes,
	faMicrochip,
	faExclamationCircle,
	faCheckCircle,
	faGitlab,
	faCogs,
	faSuitcase
);
Vue.component('vue-fontawesome', FontAwesomeIcon);

Vue.use(Vuelidate);

Vue.config.productionTip = false;

Vue.use(Buefy, {
	defaultIconComponent: 'vue-fontawesome',
	defaultIconPack: 'fas'
});

Vue.use(VueMq, {
	breakpoints: {
		xs: 450,
		mobile: 768,
		tablet: 1024,
		desktop: 1216,
		widescreen: 1408
	}
});

new Vue({
	router,
	render: h => h(App)
}).$mount('#app');
